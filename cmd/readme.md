# Commands

## How to build

Download Go from here:

https://golang.org/dl

for example with Windows:

https://golang.org/dl/go1.17.windows-amd64.zip

and extract archive. Then download Mech:

https://github.com/89z/mech/archive/refs/heads/master.zip

and extract archive. Then just navigate to `mech/cmd/youtube` and run
`go build`.
