# Reddit

ID     | height | mimeType
-------|--------|--------------
k1ov8f | 480    | AdaptationSet
fffrnw | 720    | Representation

- https://github.com/ytdl-org/youtube-dl/issues/29986
- https://github.com/ytdl-org/youtube-dl/issues/30092
